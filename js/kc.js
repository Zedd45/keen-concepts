/*!
 * Keen Concepts
 * keenwebconcepts.com | @zedd45
 * http://opensource.org/licenses/MIT
 */

(function () {

	kcModal = {

		initModal: function ( selector ) {

			this.trigger = document.querySelector( selector );
			this.dialog = document.querySelector('#kc-contact-modal');
			this.form = $( "form", this.dialog );
			this.close  = this.dialog.querySelector('.kc-icon-close');

			this.bindEventListeners();

			return this;
		},

		bindEventListeners: function () {

			this.trigger.addEventListener('click', $.proxy( this.openModal, this ));
			this.close.addEventListener('click', $.proxy( this.closeModal, this ));
			this.form.on("submit", $.proxy( this.submitForm, this ));

			$("body").on('keyup', $.proxy( this.checkKeyCode, this));
		},

		unbindEventListeners: function () {
			this.trigger.removeEventListener('click', $.proxy( this.openModal, this ));
			this.close.removeEventListener('click', $.proxy( this.closeModal, this ));
			this.form.off("submit", $.proxy( this.submitForm, this ));
			$("body").off('keyup', $.proxy( this.checkKeyCode, this ));
		},

		checkKeyCode: function (e) {
			if ( 27 === event.keycode ) { // escape
				this.closeModal();
			}
		},

		closeModal: function () {
			$( this.dialog ).removeClass('open');
		},

		openModal: function () {
			$( this.dialog ).addClass('open');

			var box = this.dialog;

			// body has no height due to positioning characteristics, and box is absolutely positioned, so... Flexbox? :D
			this.dialog.style.top = -1 * ( 375/2 ) + "px";
			this.dialog.style.left = -1 * ( 375/2 ) + "px";

			return this;
		},

		submitForm: function (e) {
			e.preventDefault();
			// prevent multiple submissions
			this.form.find("button").attr("disabled", true);
			$.post( this.form.attr("action"), this.form.serialize() , $.proxy( this.closeModal, this ));
		},

		showSuccess: function () {
			var successMsg = $('<span class="fa fa-check-circle" style="color: green;"></span>');
			this.form.hide("medium");
			this.dialog.append( successMsg );
			setTimeout( $.proxy( this.closeModal, this), 2000);
		},

		remove: function () {
			this.unbindEventListeners();

			this.trigger = null;
			this.dialog = null;
			this.close = null;
			this.form = null;
		},

	};


	document.addEventListener("DOMContentLoaded", function() {
	    kcModal.initModal('#kc-contact-form');
	});

})();


